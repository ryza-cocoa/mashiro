# Demo for mashiro


### Example
```
$ iTunesMeta [number of colors]
```

![Screenshot 1](Screenshot-1.png)

If everything is fine, you'll get iTunesStatictics.jpg

![Screenshot 2](Screenshot-2.jpg)

#### Link
My [blog post](https://ryza.moe/2016/02/using-k-means-cluster-algorithm-to-computing-the-dominant-colors-of-given-image/)
